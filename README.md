# homegit
## manage (dot)files in your home directory directly with (home)git

### Initial setup
```
alias homegit='/usr/bin/git --git-dir=$HOME/.homegit/ --work-tree=$HOME'
git init --bare ~/.homegit
homegit config status.showUntrackedFiles no
```

```
homegit init
homegit remote add origin  https://oauth2:<token>@gitlab.com/<USERNAME>/homegit.git
homegit fetch
homegit checkout origin/master -ft
```

### Use
alias homegit='/usr/bin/git --git-dir=$HOME/.homegit/ --work-tree=$HOME'

homegit status
